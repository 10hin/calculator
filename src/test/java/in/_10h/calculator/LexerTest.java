package in._10h.calculator;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.ParameterizedTest;

public class LexerTest {
    public void inputPatternTest_source() {
        return Stream.of( //
            Arguments.of("1", Optional.of(List.of(new Token("1"))), Optional.empty()), //
            Arguments.of("0", Optional.of(List.of(new Token("0"))), Optional.empty()), //
            Arguments.of("1+1", Optional.of(List.of(new Token("1"), new Token("+"), new Token("1"))), Optional.empty()), //
            Arguments.of("1 +1", Optional.of(List.of(new Token("1"), new Token("+"), new Token("1"))), Optional.empty()), //
            Arguments.of("1+ 1", Optional.of(List.of(new Token("1"), new Token("+"), new Token("1"))), Optional.empty()), //
            Arguments.of("1 + 1", Optional.of(List.of(new Token("1"), new Token("+"), new Token("1"))), Optional.empty()), //
            Arguments.of("1  +1", Optional.of(List.of(new Token("1"), new Token("+"), new Token("1"))), Optional.empty()), //
            Arguments.of("1+  1", Optional.of(List.of(new Token("1"), new Token("+"), new Token("1"))), Optional.empty()), //
            Arguments.of("1  +  1", Optional.of(List.of(new Token("1"), new Token("+"), new Token("1"))), Optional.empty()), //
            Arguments.of("++", Optional.of(List.of(new Token("+"), new Token("+"))), Optional.empty()) //
        );
    }
    private static class TestCase {
        private final String input;
        private final Optional<List<Token>> expectedReturnValue;
        private final Optional<Throwable> expectedThrownError;
        public TestCase( //
            final String input, //
            final Optional<List<Token>> expectedReturnValue, //
            final Optional<Throwable> expectedThrownError //
        ) {
            this.input = input;
            this.expectedReturnValue = expectedReturnValue;
            this.expectedThrownError = expectedThrownError;
        }
    }
    @ParameterizedTest( //
        name = "Run {index}: input = {0}, expectedReturnValue = {1}, expectedThrownError = {2}" //
    ) //
    @MethodSource("inputPatternTest_source")
    public void inputPatternTest( //
        final String input, //
        final Optional<List<Token>> expectedReturnValue, //
        final Optional<Throwable> expectedThrownError //
    ) {

        final Lexer target = new Lexer(new ByteArrayInputStream(testCase.input.getBytes(StandardCharsets.UTF_8)));
        Optional<List<Token>> actualReturnValue = null;
        Optional<Throwable> actualThrownError = null;
        try {
            final List<Token> actual = target.analyze();
            actualReturnValue = Optional.of(actual);
            actualThrownError = Optional.empty();
        } catch (final Throwable actual) {
            actualThrownError = Optional.of(actual);
            if (actualReturnValue == null) {
                actualReturnValue = Optional.empty();
            }
        }

        if (expectedReturnValue.isPresent()) {
            Assertions.assertTrue(actualReturnValue.isPresent());
            Assertions.assertEquals(expectedReturnValue.get(), actualReturnValue.get());
            return;
        } else if (expectedThrownError.isPresent()) {
            Assertions.assertTrue(actualThrownError.isPresent());
            Assertions.assertEquals(expectedThrownError.get().getClass(), actualThrownError.get().getClass());
            Assertions.assertEquals(expectedThrownError.get().getMessage(), actualThrownError.get().getMessage());
        } else {
            Assertions.fail();
        }

    }
}
