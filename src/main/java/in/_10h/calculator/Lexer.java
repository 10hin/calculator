package in._10h.calculator;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {

    private static final String NUMBER_REGEX = "[1-9][0-9]*|0";
    private static final Pattern NUMBER_STARTING_PATTERN = Pattern.compile("^" + NUMBER_REGEX);

    private static final String PLUS_OPERATOR_REGEX = "\\+";
    private static final Pattern PLUS_OPERATOR_STARTING_PATTERN = Pattern.compile("^" + PLUS_OPERATOR_REGEX);

    private static final Pattern[] TOKEN_PATTERNS = new Pattern[] {
        NUMBER_STARTING_PATTERN, //
        PLUS_OPERATOR_STARTING_PATTERN, //
    };

    // private static final Pattern MINUS_OPERATOR_PATTERN = Pattern.compile("-");

    // private static final Pattern PAREN_OPEN_PATTERN = Pattern.compile("\\(");

    // private static final Pattern PAREN_CLOSE_PATTERN = Pattern.compile("\\)");

    private static final int BUFFER_SIZE = 4_096;

    private final InputStreamReader input;

    public Lexer(final InputStream input) {
        if (input.markSupported()) {
            this.input = new InputStreamReader(input, StandardCharsets.UTF_8);
        } else {
            this.input = new InputStreamReader(new BufferedInputStream(input, BUFFER_SIZE), StandardCharsets.UTF_8);
        }
    }

    public List<Token> analyze() throws LexicalAnalizeFailedException {
        final List<Token> tokens = new ArrayList<>();
        final StringBuilder tokenCandidateBuffer = new StringBuilder();
        int c = 0;
        Context context = Context.INITIAL;
        try {
            while ((c = input.read()) != -1) {
                final char ch = (char) c;

                switch (context) {

                    case INITIAL:

                    if (Character.isWhitespace(ch)) {

                        context = Context.SKIPPING_WHITE_SPACE;

                    } else {

                        context = Context.READING_TOKEN_CANDIDATE;
                        tokenCandidateBuffer.append(ch);

                    }

                    break;

                    case SKIPPING_WHITE_SPACE:

                    if (!Character.isWhitespace(ch)) {
                        context = Context.READING_TOKEN_CANDIDATE;
                        tokenCandidateBuffer.append(ch);
                    }
                    // Nothing to do when found white space while skipping white space.

                    break;

                    case READING_TOKEN_CANDIDATE:

                    if (Character.isWhitespace(ch)) {
                        tokens.addAll(splitCandidateToToken(tokenCandidateBuffer.toString()));
                        context = Context.SKIPPING_WHITE_SPACE;
                        tokenCandidateBuffer.setLength(0);
                    } else {
                        tokenCandidateBuffer.append(ch);
                    }

                    break;

                    default:

                    throw new InternalError("this statement should not called");

                    // break;

                }
            }

        } catch (final IOException ioe) {

            throw new LexicalAnalizeFailedException("io exception occured", ioe);

        }

        return tokens;

    }

    private static enum Context {
        INITIAL,
        SKIPPING_WHITE_SPACE,
        READING_TOKEN_CANDIDATE,
        ;
    }

    private static enum TokenCandidateType {
        IGNORING,
        TOKEN_CANDIDATE,
        ;
    }

    private List<Token> splitCandidateToToken(final CharSequence tokenCandidate) {
        final List<Token> tokens = new ArrayList<>();
        final StringBuilder tokensBuffer = new StringBuilder(tokenCandidate);
        while (tokensBuffer.length() > 0) {
            boolean matched = false;
            for (Pattern tokenPattern : TOKEN_PATTERNS) {
                final Matcher matchResult;
                if ((matchResult = tokenPattern.matcher(tokensBuffer)).lookingAt()) {
                    matched = true;
                    final Token newToken = new Token( //
                        tokensBuffer.substring( //
                            matchResult.start(), //
                            matchResult.end() //
                        ) //
                    );
                    tokens.add(newToken);
                    tokensBuffer.delete( //
                        matchResult.start(), //
                        matchResult.end() //
                    );
                }
            }
            if (!matched) {
                throw new InternalError("valid token not found: \"" + tokensBuffer + "\"");
            }
        }
        return tokens;
    }

}
