package in._10h.calculator;

public class LexicalAnalizeFailedException extends RuntimeException {
    private static final long serialVersionUID = 984105646927844477L;
    public LexicalAnalizeFailedException() {
        super();
    }
    public LexicalAnalizeFailedException(final String message) {
        super(message);
    }
    public LexicalAnalizeFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }
    public LexicalAnalizeFailedException(final Throwable cause) {
        super(cause);
    }
    protected LexicalAnalizeFailedException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
