package in._10h.calculator.syntactictree;

import java.util.List;

public interface Tree {
    List<? extends Tree> branches();
}
