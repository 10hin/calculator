package in._10h.calculator.syntactictree;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import in._10h.calculator.Token;

public class NumberLiteral implements Expr {

    private static final String INTEGER_REGEX = "^-?([1-9][0-9]*|0)$";
    private static final Pattern INTEGER_PATTERN = Pattern.compile(INTEGER_REGEX);

    private final int value;
    private NumberLiteral(final Token token) throws NumberFormatException {
        this.value = Integer.parseInt(token.source());
    }
    public List<Expr> branches() {
        return Collections.emptyList();
    }
    public int evaluate() {
        return this.value;
    }
    public static Optional<NumberLiteral> parse(final Token token) {
        if (!INTEGER_PATTERN.matcher(token.source()).matches()) {
            return Optional.empty();
        }
        return Optional.of(new NumberLiteral(token));
    }
}
