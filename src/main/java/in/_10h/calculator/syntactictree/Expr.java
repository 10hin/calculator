package in._10h.calculator.syntactictree;

import java.util.List;

public interface Expr extends Tree {
    List<? extends Expr> branches();
    int evaluate();
}
