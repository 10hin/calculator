package in._10h.calculator;

public class Token {

    private final String source;

    public Token(final String source) {
        this.source = source;
    }

    public String source() {
        return this.source;
    }

    @Override
    public String toString() {
        return "Token (source: " + this.source + ")";
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof Token) {
            return false;
        }
        final Token other = (Token) obj;
        return this.source().equals(other.source());
    }

    @Override
    public int hashCode() {
        return this.source.hashCode();
    }

}
