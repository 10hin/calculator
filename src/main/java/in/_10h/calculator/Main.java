package in._10h.calculator;

import java.util.List;

public class Main {

    public static void main(final String[] args) {
        final Lexer lexer = new Lexer(System.in);
        final List<Token> tokens = lexer.analyze();
        System.out.println(tokens);
    }
    
}
